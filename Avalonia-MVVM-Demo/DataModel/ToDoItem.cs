﻿namespace Avalonia_MVVM_Demo.DataModel
{
    public class ToDoItem
    {
        public string Description { get; set; } = string.Empty;
        public bool IsChecked { get; set; }
    }
}
