﻿using Avalonia_MVVM_Demo.DataModel;
using System.Collections.Generic;

namespace Avalonia_MVVM_Demo.Services
{
    public class ToDoListService
    {
        public IEnumerable<ToDoItem> GetItems() => new[]
        {
            new ToDoItem(){Description = "111"},
            new ToDoItem(){Description = "222"},
            new ToDoItem(){Description = "333" ,IsChecked = true},
        };
    }
}
