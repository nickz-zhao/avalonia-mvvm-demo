﻿using ReactiveUI;
using System;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using Avalonia.MusicStore.Models;
using System.Linq;
using System.Threading;
using System.Reactive;

namespace Avalonia.MusicStore.ViewModels
{
    public class MusicStoreViewModel : ViewModelBase
    {
        private CancellationTokenSource? _cancellationTokenSource;
        private string? _searchText;
        private bool _isBusy;

        public string? SearchText
        {
            get => _searchText;
            set => this.RaiseAndSetIfChanged(ref _searchText, value);
        }

        public bool IsBusy
        {
            get => _isBusy;
            set => this.RaiseAndSetIfChanged(ref _isBusy, value);
        }

        private AlbumViewModel? _selectedAlbum;

        public ObservableCollection<AlbumViewModel> SearchResults { get; } = new();

        public AlbumViewModel? SelectedAlbum
        {
            get => _selectedAlbum;
            set => this.RaiseAndSetIfChanged(ref _selectedAlbum, value);
        }

        public MusicStoreViewModel()
        {
            BuyMusicCommand = ReactiveCommand.Create(() =>
            {
                return SelectedAlbum;
            });
            
            this.WhenAnyValue(x => x.SearchText)                //WhenAnyValue 方法由 ReactiveUI 框架提供，作为 ReactiveObject 的一部分（继承自 ViewModelBase）。该方法接受一个 lambda 表达式参数，该参数获取要观察的属性。因此，在上面的代码中，每当用户键入以更改搜索文本时，都会发生一个事件。
                .Throttle(TimeSpan.FromMilliseconds(400))       //Throttle 方法可以让事件在经过指定的时间间隔（400毫秒）后没有再次发生才会被处理。这意味着在用户停止键入 400 毫秒或更长时间之前，不会开始处理。
                .ObserveOn(RxApp.MainThreadScheduler)           //ObserveOn 方法是必需的，以确保订阅的方法始终在 UI 线程上调用。在 Avalonia UI 应用程序中，您必须始终在 UI 线程上更新 UI。
                .Subscribe(DoSearch!);                          //Subscribe 方法对每个观察到的事件调用 DoSearch 方法。DoSearch 方法以异步方式运行，没有返回值。
        }
        private async void DoSearch(string s)
        {
            IsBusy = true;
            SearchResults.Clear();

            _cancellationTokenSource?.Cancel();
            _cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = _cancellationTokenSource.Token;

            if (!string.IsNullOrWhiteSpace(s))
            {
                var albums = await Album.SearchAsync(s);

                foreach (var album in albums)
                {
                    var vm = new AlbumViewModel(album);

                    SearchResults.Add(vm);
                }

                if (!cancellationToken.IsCancellationRequested)
                {
                    LoadCovers(cancellationToken);
                }
            }

            IsBusy = false;
        }
        private async void LoadCovers(CancellationToken cancellationToken)
        {
            foreach (var album in SearchResults.ToList())
            {
                await album.LoadCover();

                if (cancellationToken.IsCancellationRequested)
                {
                    return;
                }
            }
        }
        public ReactiveCommand<Unit, AlbumViewModel?> BuyMusicCommand { get; }
    }
}
